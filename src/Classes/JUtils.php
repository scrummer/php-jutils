<?php

namespace JUtils;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class JUtils
 *
 * @author  Scrummer <scrummer@gmx.ch>
 * @package JUtils
 */
final class JUtils
{
    public const VERSION = '@package_version@';
    public const RELEASE_DATE = '@release_date@';
    public const APP_NAME = '@application_name@';

    /**
     * Global instance of jutils. It can be accessed only after constructor call.
     */
    private static JUtils $instance;

    private Application $console;

    public function __construct(Application $console = null)
    {
        $this->console = $console;
        self::$instance = $this;
    }

    public static function run(): void
    {
        $console = new Application(self::APP_NAME, self::VERSION);
        $input = new ArgvInput();
        $output = new ConsoleOutput();
        $jUtils = new self($console);
        $jUtils->init();
        $console->run($input, $output);
    }

    private function init(): void
    {
        $this->console->add(new \JUtils\Command\BuildTwigCommand($this));
    }

    public function getConsole(): ?Application
    {
        return $this->console;
    }
}
