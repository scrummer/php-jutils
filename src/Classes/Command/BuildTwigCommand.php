<?php

namespace JUtils\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Twig\Error\Error;

/**
 * Class BuildTwigCommand
 *
 * @author  Scrummer <scrummer@gmx.ch>
 * @package JUtils\Command
 */
class BuildTwigCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('build:twig')
            ->setDescription('Build HTML Files out of twig structure and (optional) configuration')
            ->setHelp(<<<EOF
You can set up your Twig Filestructure in a directory of your choice.
For each twig file you have the possibility to create a config file which must return an array. This array will be passed to the context of the twig file while compiling it.

If you add a preceding underscore to the filename, the file will not be compiled (can be pretty usefull when working with inheritance).

Example file structure:
/templates
|- _base.twig
|- index.twig
|- /sub
   |- subpage.twig
/build

Command to compile this structure:
$ jutils build:twig ./templates ./build
-> the files will be compiled into the ./build directory

Structure after building:
/templates
|- _base.twig
|- index.twig
|- /sub
   |- subpage.twig
/build
|- index.html
|- /sub
   |- subpage.html

Note: The index.twig extends the _base.twig. Because the _base.twig starts with an underscore, it won't be compiled.
EOF
            )
            ->addArgument('src', InputArgument::REQUIRED, 'The source directory from where the twig files are located')
            ->addArgument('dist', InputArgument::REQUIRED, 'The distribution directory to where the compiled html files will be sent');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get working dir and search for all twig files
        $workingDir = realpath($input->getArgument('src'));
        $twig = $this->getTwig([
            getcwd() . '/' . $input->getArgument('src')
        ]);
        $globalConfigFile = sprintf('%s/twigGlobals.php', $workingDir);
        $globalVars = file_exists($globalConfigFile) ? include $globalConfigFile : [];
        $finder = new Finder();
        $finder
            ->name('*.twig')
            ->in($workingDir);

        // Add global Variables to twig
        foreach ($globalVars as $variable => $value) {
            $twig->addGlobal($variable, $value);
        }

        foreach ($finder as $file) {
            // Get the file name without file extension (eg. "test.twig" => "test")
            $baseFileName = $file->getBasename('.twig');

            // check if the file starts with an underscore.
            // These files won't be compiled since they are intended to be parent/abstract files
            if (preg_match('/^_.*/', $baseFileName)) {
                continue;
            }

            // Set some shitty stuff
            $configFile = sprintf('%s/%s.php', $file->getPathInfo(), $baseFileName);
            $distDir = $input->getArgument('dist') . (!empty($file->getRelativePath()) ? '/' . $file->getRelativePath() : '');
            $distFile = sprintf('%s/%s.html', $distDir, $baseFileName);
            $config = file_exists($configFile) ? include $configFile : [];

            // Create dist directory if it doesn't exist
            if (!is_dir($distDir)) {
                $this->getFilesystem()->mkdir($distDir);
            }

            // Render that shit with twiggggggggy 🐷
            try {
                $html = $twig->render($file->getRelativePathname(), $config);
            } catch (Error $e) {
                throw new \RuntimeException(sprintf('Couldn\'t compile twig syntax: %s', $e->getMessage()));
            }

            if (!file_put_contents($distFile, $html)) {
                throw new \RuntimeException('Couldn\'t write content to new file');
            }

            $output->writeln(sprintf('<info>Successfully compiled file: %s</info>', $file->getFilename()));
        }

        $output->writeln('<info>All files were successfully compiled</info>');
        exit(0);
    }
}
