<?php

namespace JUtils\Command;

use JUtils\JUtils;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Class AbstractCommand
 *
 * @author  Scrummer <scrummer@gmx.ch>
 * @package JUtils\Command
 */
class AbstractCommand extends Command
{
    private JUtils $jutils;

    private ?array $config = null;

    private ?Environment $twig = null;

    private ?Filesystem $filesystem = null;

    public function __construct(JUtils $jutils)
    {
        $this->jutils = $jutils;
        parent::__construct();
    }

    public function getJUtils(): JUtils
    {
        return $this->jutils;
    }

    public function getNewProcess(array $command = []): Process
    {
        return new Process($command);
    }

    public function getConfig(): array
    {
        if (null === $this->config) {
            try {
                $this->config = Yaml::parse(file_get_contents(__DIR__ . '/../../config.yml'));
            } catch (ParseException $e) {
                echo(sprintf('Unable to parse the YAML string: %s', $e->getMessage()));
            }
        }

        return $this->config;
    }

    public function getTwig(array $includePaths = []): Environment
    {
        if ($this->twig === null || !empty($includePaths)) {
            $loader = new FilesystemLoader($includePaths);
            $this->twig = new Environment($loader);
        }

        return $this->twig;
    }

    public function getFilesystem(): Filesystem
    {
        if ($this->filesystem === null) {
            $this->filesystem = new Filesystem();
        }

        return $this->filesystem;
    }
}
