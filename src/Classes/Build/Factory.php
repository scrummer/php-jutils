<?php

/*
 * This file is part of Composer.
 *
 * (c) Nils Adermann <naderman@naderman.de>
 *     Jordi Boggiano <j.boggiano@seld.be>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace JUtils\Build;

class Factory
{
    /**
     * @return string
     * @throws \RuntimeException
     */
    public static function getHomeDir()
    {
        $home = getenv( 'SCRUMMER_JUTILS_HOME' );
        if ( !$home ) {
            if ( defined( 'PHP_WINDOWS_VERSION_MAJOR' ) ) {
                if ( !getenv( 'APPDATA' ) ) {
                    throw new \RuntimeException( 'The APPDATA or SCRUMMER_JUTILS_HOME environment variable must be set for jutils to run correctly' );
                }
                $home = strtr( getenv( 'SCRUMMER_JUTILS_HOME' ), '\\', '/' ) . '/jutils';
            } else {
                if ( !getenv( 'HOME' ) ) {
                    throw new \RuntimeException( 'The HOME or SCRUMMER_JUTILS_HOME environment variable must be set for jutils to run correctly' );
                }
                $home = rtrim( getenv( 'HOME' ), '/' ) . '/.jutils';
            }
        }

        if ( !file_exists( $home . '/.htaccess' ) ) {
            if ( !is_dir( $home ) ) {
                @mkdir( $home, 0777, true );
            }
            @file_put_contents( $home . '/.htaccess', 'Deny from all' );
        }

        return $home;
    }

    /**
     * @param string $home
     *
     * @return string
     */
    public static function getCacheDir( $home = null )
    {
        if ( !$home ) $home = self::getHomeDir();

        $cacheDir = getenv( 'SCRUMMER_JUTILS_CACHE_DIR' );
        if ( !$cacheDir ) {
            if ( defined( 'PHP_WINDOWS_VERSION_MAJOR' ) ) {
                if ( $cacheDir = getenv( 'LOCALAPPDATA' ) ) {
                    $cacheDir .= '/jutils';
                } else {
                    $cacheDir = $home . '/cache';
                }
                $cacheDir = strtr( $cacheDir, '\\', '/' );
            } else {
                $cacheDir = $home . '/cache';
            }
        }

        if ( !is_dir( $cacheDir ) ) {
            @mkdir( $cacheDir, 0777, true );
        }

        return $cacheDir;
    }

}
