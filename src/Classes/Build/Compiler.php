<?php

namespace JUtils\Build;

use Symfony\Component\Process\Process;
use Symfony\Component\Finder\Finder;

class Compiler
{

    private $version;

    /**
     * @var \DateTime
     */
    private $versionDate;

    /**
     * Compiles jutils into a single phar file
     *
     * @throws \RuntimeException
     *
     * @param  string $pharFile The full path to the file to create
     */
    public function compile( $pharFile = 'jutils.phar' )
    {
        if ( file_exists( $pharFile ) ) {
            unlink( $pharFile );
        }

        $process = new Process( 'git log --pretty="%H" -n1 HEAD', __DIR__ );
        if ( $process->run() != 0 ) {
            throw new \RuntimeException( 'Can\'t run git log. You must ensure to run compile from composer git repository clone and that git binary is available.' );
        }
        $hash = trim( $process->getOutput() );

        $process = new Process( 'git log -n1 --pretty=%ci HEAD', __DIR__ );
        if ( $process->run() != 0 ) {
            throw new \RuntimeException( 'Can\'t run git log. You must ensure to run compile from composer git repository clone and that git binary is available.' );
        }

        $this->versionDate = new \DateTime( trim( $process->getOutput() ), new \DateTimeZone( 'Europe/Zurich' ) );

        $process = new Process( 'git log -n1 --pretty=%ct HEAD' );
        if ( $process->run() == 0 ) {
            $this->version = trim( $process->getOutput() ) . " ($hash)";
        }

        $phar = new \Phar( $pharFile, 0 );
        $phar->setSignatureAlgorithm( \Phar::SHA1 );
        $phar->startBuffering();

        $finderSort = function ( $a, $b ) {
            return strcmp( strtr( $a->getRealPath(), '\\', '/' ), strtr( $b->getRealPath(), '\\', '/' ) );
        };

        $finder = new Finder();
        $finder->files()
            ->ignoreDotFiles( false )
            ->name( '*' )
            ->exclude( 'Test' )
            ->notName( 'Compiler.php' )
            ->in( __DIR__ . '/../..' )
            ->sort( $finderSort );

        foreach ( $finder as $file ) {
            $this->addFile( $phar, $file );
        }

        $finder = new Finder();
        $finder->files()
            ->ignoreVCS( true )
            ->name( '*.php' )
            ->name( 'api_description.json' )
            ->name( 'LICENSE' )
            ->exclude( 'tests' )
            ->exclude( 'test' )
            ->exclude( 'docs' )
            ->exclude( 'doc' )
            ->exclude( 'Tests' )
            ->exclude( 'symfony/finder' )
            ->in( __DIR__ . '/../../../vendor/composer/' )
            ->in( __DIR__ . '/../../../vendor/symfony/' )
            ->in( __DIR__ . '/../../../vendor/psr/' )
            ->in( __DIR__ . '/../../../vendor/twig' )
            ->sort( $finderSort );

        foreach ( $finder as $file ) {
            $this->addFile( $phar, $file );
        }

        $this->addFile( $phar, new \SplFileInfo( __DIR__ . '/../../../vendor/autoload.php' ) );

        $this->addComposerBin( $phar );

        // Stubs
        $phar->setStub( $this->getStub() );

        $phar->stopBuffering();

    }

    /**
     * @param \Phar  $phar
     * @param string $file
     *
     * @return void
     */
    private function addFile( $phar, $file )
    {
        $path = strtr( str_replace( realpath( __DIR__ . '/../../../' ), '', $file->getRealPath() ), '\\', '/' );
        if ( $path === '/src/Classes/JUtils.php' ) {
            $content = file_get_contents( $file );

            $content = str_replace( '@package_version@', $this->version, $content );
            $content = str_replace( '@application_name@', 'Scrummer php-jutlis', $content );
            $content = str_replace( '@release_date@', $this->versionDate->format( 'Y-m-d H:i:s' ), $content );

            $phar->addFromString( $path, $content );
        } else {
            $phar->addFile( $file, $path );
        }

    }

    /**
     * @param \Phar $phar
     */
    private function addComposerBin( $phar )
    {
        $content = file_get_contents( __DIR__ . '/../../../bin/jutils' );
        $content = preg_replace( '{^#!/usr/bin/env php\s*}', '', $content );
        $phar->addFromString( 'bin/jutils', $content );
    }

    private function getStub()
    {
        return <<<'EOF'
#!/usr/bin/env php
<?php
/*
 * This file is part of Scrummer's php-jutils.
 *
 */

Phar::mapPhar('jutils.phar');

require 'phar://jutils.phar/bin/jutils';

__HALT_COMPILER();
EOF;
    }


}
